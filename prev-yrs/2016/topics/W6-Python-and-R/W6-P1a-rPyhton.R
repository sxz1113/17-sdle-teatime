## requirement R version >3.2
## requirement python version > 2.7

## launch R in command line
## install rPython package from cran
## alternative way: sudo pip install rpython

library(rPython)
a = 1:4
b = 5:8
 
## python.assign: assign values to variables in Python as well as get their values back to R

python.assign("c",a)
## python.exec(): execute a python command
length(a)
python.exec('d=len(c)')
## python.get(): get variable value
python.get('d')

## python.call: call python function from R

python.exec(c('def summation_list(a,b):','\t return sum(a+b)'))
python.call('summation_list',a,b)









