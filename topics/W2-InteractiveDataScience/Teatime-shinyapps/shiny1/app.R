#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
ui <- (fluidPage(
  
  titlePanel("My First Shiny App!"),
  tags$a(href="https://shiny.rstudio.com/reference/shiny/latest/titlePanel.html", "Shiny"),
  sidebarLayout(
    sidebarPanel(
      #*Input() functions
      textInput("myText", "Enter text here:")
    ),
    mainPanel(
      #*Output() functions
      textOutput("niceTextOutput")
    )
  )
))



# Define server logic required to output entered text
server <- (function(input, output){ output$niceTextOutput <- renderText(paste("You entered the text:\n", input$myText)) })

# Run the application 
shinyApp(ui = ui, server = server)

